#import "GameScreen.h"

#define d2r(x) ((x) / 180.0 * 3.1415926535)
#define r2d(x) ((x) * 180.0 / 3.1415926535)

#define BALL_SIZE 10.0
#define PADDLE_SIZE 30.0
#define BRICK_SIZE 20.0
#define BALL_RAD (BALL_SIZE/2)
#define PADDLE_RAD (PADDLE_SIZE/2)
#define BRICK_RAD (BRICK_SIZE/2)

@implementation GameScreen
@synthesize paddle, ball, bricks;
@synthesize stock1, stock2, stock3;
@synthesize timer;

-(void)createPlayField {
    CGRect bounds = [self bounds];
    paddle = [[UIView alloc] initWithFrame:CGRectMake(bounds.size.width/2 - PADDLE_RAD, bounds.size.height - PADDLE_SIZE, PADDLE_SIZE, PADDLE_SIZE)];
    paddle.layer.cornerRadius = PADDLE_RAD;
    paddle.backgroundColor = [UIColor blackColor];
    [self addSubview:paddle];

	ball = [[UIView alloc] initWithFrame:CGRectMake(bounds.size.width/2 - BALL_RAD, BALL_SIZE, BALL_SIZE, BALL_SIZE)];
    ball.layer.cornerRadius = BALL_RAD;
    ball.backgroundColor = [UIColor redColor];
    [self addSubview:ball];

    stock1 = [[UIView alloc] initWithFrame:CGRectMake(bounds.size.width/2 - BALL_RAD, BALL_SIZE, BALL_SIZE, BALL_SIZE)];
    stock1.layer.cornerRadius = BALL_RAD;
    stock1.backgroundColor = [UIColor redColor];

    stock2 = [[UIView alloc] initWithFrame:CGRectMake(bounds.size.width/2 - BALL_RAD, BALL_SIZE, BALL_SIZE, BALL_SIZE)];
    stock2.layer.cornerRadius = BALL_RAD;
    stock2.backgroundColor = [UIColor redColor];

    stock3 = [[UIView alloc] initWithFrame:CGRectMake(bounds.size.width/2 - BALL_RAD, BALL_SIZE, BALL_SIZE, BALL_SIZE)];
    stock3.layer.cornerRadius = BALL_RAD;
    stock3.backgroundColor = [UIColor redColor];

    bricks = [[NSMutableArray alloc] init];
    stocks = 0;
}

-(void)reset {
    CGRect bounds = [self bounds];
    [paddle setCenter: CGPointMake(bounds.size.width/2, bounds.size.height - PADDLE_SIZE * 2)];
    [ball setCenter: CGPointMake(bounds.size.width/2, BALL_SIZE)];
    [stock1 setCenter: CGPointMake(bounds.size.width/2 - BALL_SIZE * 3, bounds.size.height - BALL_SIZE)];
    [stock2 setCenter: CGPointMake(bounds.size.width/2, bounds.size.height - BALL_SIZE)];
    [stock3 setCenter: CGPointMake(bounds.size.width/2 + BALL_SIZE * 3, bounds.size.height - BALL_SIZE)];

    if(stocks > 0) {
        [stock1 removeFromSuperview];
    }
    if(stocks > 1) {
        [stock2 removeFromSuperview];
    }
    if(stocks > 2) {
        [stock3 removeFromSuperview];
    }

    for(UIView *brick in bricks) {
        [brick removeFromSuperview];
    }
    [bricks removeAllObjects];

    UIView *brick;
    for(int row=0; row<6; row++) {
        for(int num=0; num<(row+1)*(row+1); num++) {
            float x, y;
            x = bounds.size.width/2;
            y = bounds.size.height/2;

            float a = 360.0 * num/(row+1)/(row+1);

            x += cosf(d2r(a)) * BRICK_SIZE * 1.5 * row;
            y += sinf(d2r(a)) * BRICK_SIZE * 1.5 * row;

            brick = [[UIView alloc] initWithFrame: CGRectMake(x - BRICK_RAD, y - BRICK_RAD, BRICK_SIZE, BRICK_SIZE)];
            brick.layer.cornerRadius = BRICK_RAD;
            brick.backgroundColor = [UIColor blueColor];
            [bricks addObject: brick];
            [self addSubview: brick];
        }
    }

    [self addSubview: stock1];
    [self addSubview: stock2];
    [self addSubview: stock3];

    speed = 2;
    angle = 90;
    accel = 1.01;
    collided = false;
    stocks = 3;
    ball.alpha = 1.00;
}

-(void)lose {
    [self stopAnimation: NULL];
    [self reset];
}
-(void)win {
    [self stopAnimation: NULL];
    [self reset];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	for (UITouch *t in touches) {
		CGPoint p = [t locationInView:self];
		[paddle setCenter:p];
	}
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	[self touchesBegan:touches withEvent:event];
}

-(IBAction)startAnimation:(id)sender {
	timer = [NSTimer scheduledTimerWithTimeInterval:.01666	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    [self reset];
}

-(IBAction)stopAnimation:(id)sender {
	[timer invalidate];
}

-(void)timerEvent:(id)sender {
	CGRect bounds = [self bounds];

	CGPoint b = [ball center];
    CGPoint p = [paddle center];
    bool speedup = false;

    // wall collisions
    if(b.x < BALL_RAD || b.x > bounds.size.width - BALL_RAD || b.y < BALL_RAD || b.y > bounds.size.height - BALL_RAD) {
        if(ball.alpha >= 1.00) {
            ball.alpha = 0.00;
            stocks--;
            if(stocks == 0) {
                [stock1 removeFromSuperview];
            }
            if(stocks == 1) {
                [stock2 removeFromSuperview];
            }
            if(stocks == 2) {
                [stock3 removeFromSuperview];
            }
        }
    }
    if(ball.alpha < 1.00) {
        ball.alpha += 0.01;
    }

    if(b.x < BALL_RAD) {
        if(angle >= 180 && angle <= 270) {
            angle = 540 - angle;
        }
        else if(angle >= 90 && angle < 180) {
            angle = 180 - angle;
        }
        b.x = BALL_RAD;
        speedup = true;
    }

    if(b.x > bounds.size.width - BALL_RAD) {
        if(angle >= 270 && angle <= 360) {
            angle = 540 - angle;
        }
        else if(angle >= 0 && angle <= 90) {
            angle = 180 - angle;
        }
        b.x = bounds.size.width - BALL_RAD;
        speedup = true;
    }

    if(b.y < BALL_RAD) {
        angle = 360 - angle;
        b.y = BALL_RAD;
        speedup = true;
    }

    if(b.y > bounds.size.height - BALL_RAD) {
        angle = 360 - angle;
        b.y = bounds.size.height - BALL_RAD;
        speedup = true;
    }

    // paddle collision
    float dx, dy;
    dx = p.x - b.x;
    dy = p.y - b.y;

    if(sqrtf(dx*dx + dy*dy) <= BALL_RAD + PADDLE_RAD) {
        if(!collided) {
            collided = true;
            speedup = true;
            angle = r2d(atan2f(dy, dx)) + 180;
        }
    }
    else {
        collided = false;
    }

    // brick collisions
    NSUInteger x;
    for(x=0; x<bricks.count; x++) {
        UIView *brick = bricks[x];
        dx = [brick center].x - b.x;
        dy = [brick center].y - b.y;
        if(sqrtf(dx*dx + dy*dy) <= BALL_RAD + BRICK_RAD) {
            speedup = true;
            angle = r2d(atan2f(dy, dx)) + 180;
            [bricks removeObjectAtIndex: x];
            [brick removeFromSuperview];
            x--;
        }
    }

    if(speedup) {
        speed *= accel;
    }

    while(angle < 0) {
        angle += 360;
    }
    while(angle > 360) {
        angle -= 360;
    }

    dx = cosf(d2r(angle)) * speed;
    dy = sinf(d2r(angle)) * speed;
	b.x += dx;
	b.y += dy;
	[ball setCenter:b];

    if(stocks == 0) {
        [self lose];
    }
    else if(bricks.count == 0) {
        [self win];
    }
}

@end
