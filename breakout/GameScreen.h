#import <UIKit/UIKit.h>

@interface GameScreen : UIView
{
	float angle, speed, accel;
    bool collided;
    int stocks;
}
@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball, *stock1, *stock2, *stock3;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic, strong) NSTimer *timer;

-(void)createPlayField;
-(void)reset;
-(void)win;
-(void)lose;

@end
